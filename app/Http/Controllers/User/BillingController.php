<?php

namespace App\Http\Controllers\User;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\recordPayPalTransaction;
use App\Models\ProfileVerification;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Exception\PayPalConnectionException;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use PayPal\Api\OpenIdTokeninfo;
use PayPal\Api\OpenIdUserinfo;
use PayPal\Api\PaymentExecution;

class BillingController extends Controller
{
    public $apiContext;
    public function __construct(){
        $this->middleware('auth');

        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(config('paypal.client_id') , config('paypal.secret_id'))
        );

        $this->apiContext->setConfig(
            array(
                'mode' => 'sandbox',
                'http.ConnectionTimeOut' => 300,
                'http.Retry'=> 10,
                'log.LogEnabled' => true,
                'log.FileName' => storage_path() . '/logs/paypal.log',
                'log.LogLevel' => 'FINE'
            )
        );
    }

    public function index(Request $request){
        if($request->isMethod('post')){
            $payment_mode = $request->input('payment_mode');
            $choice = "PayPal";
            if($payment_mode == 'card_payment'){
                $choice = "creditCard";
            }elseif($payment_mode == 'paypal'){
                $choice = 'PayPal';
            }elseif($payment_mode == 'orange_payment'){
                $choice = 'OrangeMoney';
            }elseif($payment_mode == 'mtn_payment'){
                $choice = 'MtnMoMo';
            }
            return redirect()->action('User\BillingController@index_choice', ['choice'=>$choice]);
        }
        return view('billing.index');
    }

    public function index_choice(Request $request, $choice){
        $data['payment_mode'] = $choice;
        return view('billing.index_choice', $data);
    }

    public function index_deposit(Request $request){
        if($request->isMethod('post')){
            $payment_mode = $request->input('payment_mode');

            if(($payment_mode == 'OrangeMoney') || ($payment_mode == 'MtnMoMo')){
                Validator::make($request->input(),[
                    'amount' => 'required|min:5000|integer'
                ])->validate();
            }else{
                Validator::make($request->input(),[
                    'amount' => 'required|min:10|integer'
                ],[
                    'amount.min'=> 'The minimum deposit amount is $10',
                    'amount.integer' => 'The expected value is an Integer type'
                ])->validate();

                $amount = $request->input('amount');
                if($payment_mode =='PayPal'){
                    $redirect = $this->paypal_execute_payment($amount);
                    return redirect()->away($redirect);
                }
            }
            
            
            /*$choice = "PayPal";
            if($payment_mode == 'card_payment'){
                $choice = "creditCard";
            }elseif($payment_mode == 'paypal'){
                $choice = 'PayPal';
            }elseif($payment_mode == 'orange_payment'){
                $choice = 'OrangeMoney';
            }elseif($payment_mode == 'mtn_payment'){
                $choice = 'MtnMoMo';
            }*/
        }
    }

    /*public function paypal_post_payment(Request $request){
        if($request->isMethod('post')){
            Validator::make($request->input(), [
                'lb_amount' => 'required|min:10'
            ],[
                'lb_amount.min'=> 'The minimum deposit amount is $10'
            ])->validate();
            $amount = $request->input('amount');
            $this->paypal_execute_payment($amount);
        }
    }*/

    private function paypal_execute_payment($a){
        
        //dd($apiContext);
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new Amount();
        $amount->setTotal(intval($a));
        $amount->setCurrency('USD');

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription("Loading money to AWT Account");

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url('/user/paypal/return?success=true'))
            ->setCancelUrl(url('/user/paypal/return?success=false'));

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);

        try {
            $payment->create($this->apiContext);

            $approvalUrl = $payment->getApprovalLink();

            return $approvalUrl;
            /*foreach($payment->getLinks() as $link) {
                if($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            echo $redirect_url;
            Session::put('paypal_payment_id', $payment->getId());

            if(!isset($redirect_url)) {
                // redirect to paypal
                return redirect()->away($redirect_url);
            }*/

            /*echo "Payment Id is " . $payment->getId();
    //exit;
            //return redirect()->away($payment->getApprovalLink());
            dd($payment);
            echo $payment . "dldsklds";
        
            echo "\n\nRedirect user to approval_url: " . $payment->getApprovalLink() . "\n";*/
        }
        catch (PayPalConnectionException $ex) {
            // This will print the detailed information on the exception.
            //REALLY HELPFUL FOR DEBUGGING
            dd($ex);
            echo $ex->getData();
            echo "Je suiis la";
        }
    }

    public function paypal_payment_status(Request $request){
        $full_url = $request->fullUrl();
        $success = $request->input('success');
        if($success){
            //$user = OpenIdUserinfo::getUserinfo(array('access_token' => $request->input('token')), $this->apiContext);
            $paymentId = $paymentId = $request->input('paymentId');

            //dd($this->apiContext);
           
            $payerId = $PayerID = $request->input('PayerID');
    
            // Execute payment with payer id
            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);
            
            try {
                $payment = Payment::get($paymentId, $this->apiContext);
                // Execute payment
                $result = $payment->execute($execution, $this->apiContext);

                /*try {
                    $payment = Payment::get($paymentId, $this->apiContext);
                } catch (Exception $ex) {
                    ResultPrinter::printError("Get Payment", "Payment", null, null, $ex);
                    exit(1);
                }*/             
                $user = User::find(Auth::user()->id);
                $top = $user->topup;
                $transaction = $result->getTransactions();
                foreach($transaction as $key){
                    $top = $top +  doubleval($key->amount->total);
                }
                $user->topup = $top;
                if($user->save()){

                }else{

                }

                $profileverification = ProfileVerification::where(['lb_user_id' => Auth::user()->id])->first();
                $profileverification->lb_topup = 1;
                $profileverification->save();

                // Save transaction history 
                $record = new recordPayPalTransaction();
                $record->lb_user_id = Auth::user()->id;
                $record->lb_transaction_id = $result->id;
                $record->lb_payer_email = $result->payer->payer_info->email;
                $record->lb_payer_firstname = $result->payer->payer_info->first_name;
                $record->lb_payer_lastname = $result->payer->payer_info->last_name;
                $record->lb_payer_id = $result->payer->payer_info->payer_id;
                $record->lb_montant = $top - $user->topup;
                $record->save();

                return redirect()->action('User\BillingController@top_up_load_success')->with(['status' => 'success']);
            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                echo $ex->getCode();
                echo $ex->getData();
                die($ex);
            } catch (Exception $ex) {
                die($ex);
            }
        }else{
echo "Failed";
        }
    }

    public function top_up_load_success(Request $request){
        return view('billing.confirm_top_up');
    }

    public function after_cancel(){
echo "Your cancel";
    }

    public function after_success(){
echo "Done well";
    }
}
