<?php

namespace App\Http\Controllers\User;

use Auth;
use App\Models\Country;
use App\Models\WoodType;
use App\Models\Port;
use App\Models\Order;
use App\Models\Inbox;
use App\Models\OrderSerialize;
use App\Models\CompanyProfile;
use App\Models\ProfileVerification;
use App\Models\LegalForm;
use App\Mail\QuotePlaced;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $data['myoffers'] = Order::select(
            'orders.id as ORDERID',
            'orders.lb_user_id as ORDERUSERID',
            'orders.lb_type_post as ORDERPOST',
            'orders.lb_wood_type_id as ORDERTYPE',
            'orders.lb_quantity as ORDERQUANTITY',
            'orders.lb_subject as ORDERSUBJECT',
            'orders.lb_desc as ORDERDESC',
            'orders.lb_picture as ORDERPICTURE',
            'woodtype.lb_type_name as WTNAME',
            'country.lb_country_name as COUNTRYNAME',
            'ports.lb_port_name as PORTNAME'
            )
        ->join('woodtype', 'woodtype.id', '=', 'orders.lb_wood_type_id')
        ->join('country', 'country.id', '=', 'orders.lb_country_id')
        ->join('ports', 'ports.id', '=', 'orders.lb_delivery_port_id')
        ->where('orders.lb_user_id', Auth::id())->orderby('orders.id', 'DESC')->get();

        return view('user.dashboard_home', $data);
    }

    public function index_after_submit_buy(){
        $data['myoffers'] = Order::where(['orders.lb_user_id' => Auth::id(), 'orders.lb_type_post' => 'buy'])
        ->join('country', 'country.id', '=', 'orders.lb_country_id')
        ->orderby('orders.id', 'DESC')->get();
        return view('user.after_submit_buy', $data);
    }

    public function index_buy(Request $request){
        if($request->isMethod('post')){
            Validator::make($request->input(), [
                'lb_country_id' => 'required',
                'lb_subject' => 'required|string|max:250',
                'lb_desc' => 'required|string'
            ])->validate();

            $order = new Order();
            $order->lb_user_id = Auth::id();
            $order->lb_subject = $request->input('lb_subject');
            $order->lb_desc = $request->input('lb_desc');
            $order->lb_country_id = $request->input('lb_country_id');
            $order->lb_type_post = $request->input('order_type');
            
            $order->lb_wood_type_id = 0;
            $order->lb_quantity = 0;
            $order->lb_delivery_port_id = 0;
            $order->lb_picture = "na";

            if($order->save()){
                $request->session()->flash('order_offer_buy_success', 'Your offer has registered successfuly. You will shortly receive notification from seller');
                return redirect()->action('User\DashboardController@index');
            }else{
                $request->session()->flash('order_offer_buy_fail', 'An error occured while registering the order');
            }
        }
        $data['countries'] = Country::orderby('lb_country_name', 'ASC')->get();
        $data['type'] = WoodType::where('lb_display', 1)->get();
        return view('user.buy_item', $data);
    }

    public function index_sell(Request $request){
        if($request->isMethod('post')){
            Validator::make($request->input(), [
                'lb_wood_type_id' => 'required|integer',
                'lb_quantity' => 'required|integer',
                'lb_country_id' => 'required',
                'lb_delivery_port_id' => 'required',
                'lb_subject' => 'required|string|max:250',
                'lb_desc' => 'required|string'
            ])->validate();
            $path = $request->file('lb_picture')->store('public/tosell');
            $path = str_replace('public/', '', $path);

            $order = new Order();
            $order->lb_type_post = $request->input('order_type');
            $order->lb_user_id = Auth::id();
            $order->lb_wood_type_id = $request->input('lb_wood_type_id');
            $order->lb_quantity = $request->input('lb_quantity');
            $order->lb_country_id = $request->input('lb_country_id');
            $order->lb_delivery_port_id = $request->input('lb_delivery_port_id');
            $order->lb_subject = $request->input('lb_subject');
            $order->lb_desc = $request->input('lb_desc');
            $order->lb_picture = $path;
            if($order->save()){
                // Send mail to all registered people looking for sales
            }else{
                $request->session()->flash('post_selling_failed', 'An error occured while trying to save the data. Verify and submit again');
            }
        }
        $data['type'] = WoodType::where('lb_display', 1)->get();
        $data['countries'] = Country::orderby('lb_country_name', 'ASC')->get();
        return view('upload_item', $data);
    }

    public function inbox(Request $request, $order_id){
        $lb_user_id = Order::find(decrypt($order_id))->lb_user_id;

        if(!$lb_user_id){
            return redirect()->action('index');
        }
        $data['lb_user_id'] = encrypt($lb_user_id);
        $data['lb_order_id'] = $order_id;
        
        return view('user.inbox', $data);
    }

    public function inbox_submit(Request $request){
        if($request->isMethod('post')){
            Validator::make($request->input(), [
                'message_to_submit' => 'required|min:20',
            ])->validate();
            
            $inbox = new Inbox();
            $inbox->lb_receiver_id = decrypt($request->input('lb_receiver_id'));
            $inbox->lb_sender_id = Auth::id();
            $inbox->lb_order_id = decrypt($request->input('lb_order_id'));
            $inbox->message = $request->input('message_to_submit');
            if(!$inbox->save()){
                $request->session()->flash('posting_message_error', 'An error occured while sending the message');
            }else{
                $request->session()->flash('posting_message_success', 'Your message is being processing');
                $order = Order::join('users', 'users.id', '=', 'orders.lb_user_id')
                ->select('users.name as FULLNAME', 'orders.lb_subject as SUBJECT', 'orders.id as ORDERID')
                ->find(decrypt($request->input('lb_order_id')));
  
                $orderSerialize = new OrderSerialize($order->ORDERID, $order->FULLNAME, $order->SUBJECT, $request->input('message_to_submit') );
                //var_dump(unserialize($orderSerialize->get()));
                Mail::to("gabrielkwaye@gmail.com")->send(new QuotePlaced(unserialize($orderSerialize->get())));
            }
            return view('user.inbox_submit');
        }
    }

    public function getCountry(Request $request, $id){
        return response()->json(Country::where('id', decrypt($id))->first());
    }

    public function getCountries(Request $request){
        return response()->json(Country::get());
    }

    public function getPorts(Request $request, $country_id){
        return response()->json(Port::where('lb_country_id', $country_id)->orderby('lb_port_name', 'ASC')->get());
    }

    public function company_profile(Request $request){
        if($request->isMethod('post')){
            Validator::make($request->input(), [
                'business_name' => 'required|max:250',
                'trade_number' => 'required|max:250',
                'tax_payer_id' => 'required|max:250',
                'legal_form' => 'required|max:250',
                /*'url_logo' => 'required|max:250',*/
                'creation_date' => '',
                'email' => 'required|email|max:250',
                'phone_contact' => 'required|max:250',
                'country' => 'required|integer',
                /*'url_trade_number' => 'required|max:250',
                'url_tax_payer' => 'required|max:250'*/
            ])->validate();

            if(null != $request->file('url_logo')){
                $path = $request->file('url_logo')->store('public/companyprofile');
                $url_logo = str_replace('public/', '', $path);
            }
            if( null != $request->file('url_trade_number')){
                $path = $request->file('url_trade_number')->store('public/companyprofile');
                $url_trade_number = str_replace('public/', '', $path);
            }
            if( null != $request->file('url_tax_payer')){
                $path = $request->file('url_tax_payer')->store('public/companyprofile');
                $url_tax_payer = str_replace('public/', '', $path);
            }
            
            $myCompany = CompanyProfile::where('lb_user_id', Auth::user()->id)->first();

            if(!$myCompany){
                $myCompany = new CompanyProfile();
                $myCompany->url_logo = $url_logo;
                $myCompany->url_trade_number = $url_trade_number;
                $myCompany->url_tax_payer = $url_tax_payer;
            }
            $myCompany->lb_user_id = Auth::user()->id;
            $myCompany->business_name = $request->input('business_name');
            $myCompany->trade_number = $request->input('trade_number');
            $myCompany->tax_payer_id = $request->input('tax_payer_id');
            $myCompany->legal_form = $request->input('legal_form');
            //$myCompany->url_logo = $url_logo;
            $myCompany->creation_date = $request->input('creation_date');
            $myCompany->email = $request->input('email');
            $myCompany->phone_contact = $request->input('phone_contact');
            $myCompany->country = $request->input('country');
            $myCompany->city = 0;
            /*$myCompany->url_trade_number = $url_trade_number;
            $myCompany->url_tax_payer = $url_tax_payer;*/

            if($myCompany->save()){
                $request->session()->flash('company_update_success', 'Your data has been submitted successfuly');

                $verification = ProfileVerification::where('lb_user_id', Auth::user()->id)->first();
                if(!$verification){
                    $verification = new ProfileVerification();
                    $verification->lb_user_id = Auth::user()->id;
                }
                $verification->lb_profile_info = 1;
                $verification->save();
            }else{
                $request->session()->flash('company_update_fail', 'Your data has been submitted successfuly');
            }
        }
        $myCompany = CompanyProfile::where('lb_user_id', Auth::user()->id)->first();
        if(!$myCompany){
            $myCompany = new \stdClass();
            $myCompany->business_name = '';
            $myCompany->trade_number = '';
            $myCompany->tax_payer_id = '';
            $myCompany->legal_form = '';
            $myCompany->url_logo = '';
            $myCompany->creation_date = '';
            $myCompany->email = '';
            $myCompany->phone_contact = '';
            $myCompany->country = '';
            $myCompany->city = '';
            $myCompany->url_trade_number = '';
            $myCompany->url_tax_payer = '';
        }
        $data['myCompany'] = $myCompany;
        $data['legal_form'] = LegalForm::get();
        $data['countries'] = Country::get();
        return view('auth.company_profile', $data);
    }
}
