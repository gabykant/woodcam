<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function hint_link($post_id){

    }

    public function getAccessToken(){
        $url = config('paypal.request_token') . "/v1/oauth2/token";
        $credentials = config('paypal.client_id') . ':' . config('paypal.secret_id');
        $header = array('Accept: application/json', 'Accept-Language: en_US');
        $fields = http_build_query([
            'grant_type' => 'client_credentials'
        ]);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, "$credentials");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($ch);
        if( $result === false){
            //echo curl_error($ch);
        }else{
            //var_dump($result);
        }
        //close connection
        curl_close($ch);

        $r = json_decode($result);
        return $r->access_token;
    }
}
