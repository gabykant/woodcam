<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Models\Role;
use App\Models\Country;
use App\Models\ProfileVerification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/account/creation';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm(){
        $data['countries'] = Country::get();
        return view('auth.register', $data);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role_slug_name' => Role::where('slug_name', 'simple_user')->first(['slug_name'])->slug_name,
        ]);
    }

    protected function registered(Request $request, $user)
    {
        $check = ProfileVerification::where("lb_user_id", $user->id)->first();
        if($check) 
            return;
        $profile = new ProfileVerification();
        $profile->lb_user_id = $user->id;
        if($profile->save()){
            // Write to log file
        }else{
            // Write to log file
        }

        Auth::logout();

        $request->session()->flash('account_creation_success');
    }
}
