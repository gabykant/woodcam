<?php

namespace App\Http\Controllers;

use App\Models\Contactus;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\WoodType;
use App\Models\Hint;
use App\User;
use App\Models\ProfileVerification;
use Illuminate\Support\Facades\Validator;

class GuestController extends Controller
{
    public function index(){
        $data['woodtype'] = WoodType::where('lb_display', 1)->get();
        $data['orders'] = Order::orderby('updated_at', 'DESC')->limit(8)->get();
        return view('welcome', $data);
    }

    public function display_order(Request $request, $order_id){
        if($request->isMethod('get')){
            $order_id = decrypt($order_id);
            $hint = new Hint();
            $hint->lb_order_id = $order_id;
            if(!$hint->save()){
                // Write to log file
            }

            $order = Order::find($order_id);
            $data['order'] = $order;
            
            $data['user'] = User::join('country', 'country.id', '=', 'users.lb_country_id')
            ->select('users.id as USERID', 'users.created_at as CREATEDAT', 'users.name as USERNAME' ,'country.lb_country_name as COUNTRY')
            ->find($order->lb_user_id);

            $data['ProfileVerification'] = ProfileVerification::where("lb_user_id", $order->lb_user_id)->first();

            return view('display_order', $data);
        }
    }

    public function contact_us(Request $request){
        if($request->isMethod('post')){
            Validator::make($request->input(),
            [
                'fullname' => 'required',
                'lb_email' => 'required|email',
                'lb_subject' => 'required',
                'lb_text' => 'required'
            ])->validate();

            $array = $request->input();
            $contactus = new Contactus();
            $contactus->fullname = $array['fullname'];
            $contactus->lb_email = $array['lb_email'];
            $contactus->lb_subject = $array['lb_subject'];
            $contactus->lb_text = $array['lb_text'];
            if($contactus->save()){
                $request->session()->flash('save_contact_success', 'Your message has been submitted successfuly ');
            }else{
                $request->session()->flash('save_contact_fail', 'An error occured while submitting your message ');
            }
        }
        return view('contact');
    }

    public function about_us(){
        return view('about');
    }

    public function search_item(Request $request){
        if($request->isMethod('post')){
            $keyword = $request->input('keyword');
            $orders = Order::where('lb_desc', "LIKE", "%$keyword%")->orderby('updated_at', 'DESC')->get();
        }else{
            $orders = Order::orderby('updated_at', 'DESC')->get();
        }
        var_dump($orders);
    }

    public function get_wood_type(Request $request){
        
    }

    public function tips(){
        return view('tips');
    }

    public function forbidden(){
        return view('forbidden');
    }

    public function account_creation(Request $request){
        if(!$request->session()->get('account_creation_success')){
            return redirect()->action('GuestController@index');
        }
        return view('auth.confirm_registration');
    }
}
