<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderSerialize extends Model
{
    public $order_id;
    public $poster_fullname;
    public $lb_subject;
    public $message;

    public function __construct($order_id, $poster_fullname, $subject, $message){
        $this->order_id = $order_id;
        $this->poster_fullname = $poster_fullname;
        $this->lb_subject = $subject;
        $this->message = $message;
    }

    public function get(){
        return serialize($this);
    }
}
