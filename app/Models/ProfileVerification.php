<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileVerification extends Model
{
    protected $table  = "profileverification";
}
