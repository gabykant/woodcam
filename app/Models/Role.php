<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $primaryKey = "slug_name";

    protected $fillable=['slug_name'];

    public function users(){
        return $this->hasMany('App\User');
    }

    public function getSlugNameAttribute($value){
        return $value;
    }
}
