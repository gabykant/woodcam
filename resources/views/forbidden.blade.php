@extends('layouts.welcome')

@section('content')

<div class="container">
    <h3>Forbidden woods in exportation
        <span class="pull-right"></span>
    </h3>
    <p>In Cameroon, there are certains woods that are forbidden in exportation as a grume
        Such woods can only exportation as cut<br /><br />
    </p>
    <div class="row">
        <div class="col-md-4">
            <ul>
                <li>IROKO</li>
                <li>MOVINGUI</li>
                <li>MOABI</li>
                <li>SIPO</li>
                <li>PADOUK</li>
                <li>BUBINGA</li>
                <li>APA / PACHILOBA</li>
                <li>BETE</li>
                <li>DOUSSIE</li>
                <li>BOSSE</li>
                <li>ACCAJOU</li>
            </ul>
        </div>
        <div class="col-md-4">
            <ul>
                <li>ASSAMELA / AFROMOSIA</li>
                <li>BIBOLO / DIBETOU</li>
                <li>ZINGANA</li>
                <li>OVENGKOL</li>
                <li>DOUKA / MAKORE</li>
                <li>PAO ROSA</li>
                <li>WENGUE</li>
                <li>ANIEGRE</li>
                <li>ABAM / LONGHI</li>
                <li>ILOMBA</li>
                <li>FROMAGER / CEIBA</li>
                <li>SAPELI</li>
            </ul>
        </div>
        <div class="col-md-4">
            image
        </div>
    </div>
    
</div>
<!-- End Section display items -->
@endsection