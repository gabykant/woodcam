@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading">Search filter</div>
                <div class="panel-body">
                    <form class="form-horizontal" action="{{ route('searchItemPost') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('lb_subject') ? ' has-error' : '' }}">
                            <label for="lb_subject">Keywords</label>
                            <input type="text" name="lb_subject" class="form-control" value="{{ old('lb_subject') }}" />
                            @if ($errors->has('lb_subject'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('lb_subject') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group btn-message">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-warning">
                                    Search
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">My offers</div>

                <div class="panel-body">
                    <?php $type_order = "Buy" ; ?>
                    @if(count($myoffers) > 0)
                        @foreach($myoffers as $offer)
                            @if($offer->ORDERPOST == 'sell')
                                <?php $type_order = 'Sell'; ?>
                            @endif

                            <div class="box-offer">
                                <h3>{{ $type_order }} : {{ $offer->ORDERSUBJECT }} ({{ $offer->WTNAME }})</h3>
                                <p> {{ $offer->ORDERDESC }}</p>
                                <p> 
                                    Country: {{ $offer->COUNTRYNAME }} / 
                                    Delivery Port: {{ $offer->PORTNAME }}
                                </p>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
