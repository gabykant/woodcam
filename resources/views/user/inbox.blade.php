@extends('layouts.inbox')

@section('content')
<div id="inbox-section">
    <div class="container">
        <div class="row">
            <div class="inside-box">
                <form action="{{ url('/user/inbox/add') }}" method="post" class="form-horizontal">
                    {{ csrf_field() }}
                    <input type="hidden" name="lb_receiver_id" value="{{ $lb_user_id }}" />
                    <input type="hidden" name="lb_order_id" value="{{ $lb_order_id }}" />
                    <div class="row">
                        <span>User anme and company</span>
                    </div>

                    <!-- Information sur le produit -->
                    <div class="row">
                        <span>Information on the project</span>
                        <br />
                        <p>
                            Write the title here
                        </p>
                    </div>

                    <div class="row">
                        <textarea id="message_to_submit" name="message_to_submit"></textarea>
                    </div>

                    <div class="row">
                        <div class="btnSendRequest pull-right">
                            <button class="btn btn-warning">Send a request now</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection