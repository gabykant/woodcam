@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading">My offers</div>
                <div class="panel-body">
                    <ul>
                        <li>
                            <a href="{{ route('buyItemPost') }}">My offer to sell</a>
                        </li>
                        <li>
                            <a href="{{ url('/user/dashboard/list_buy') }}">My offer to buy</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Offer to buy
                    <span class="pull-right">
                        <a href="{{ url('/user/dashboard/deal') }}" class="label label-warning"><i class="glyphicon glyphicon-plus"></i>Add offer</a></span>
                </div>

                <div class="panel-body">
                    <?php $type_order = "Buy" ; ?>
                    @if(count($myoffers) > 0)
                        @foreach($myoffers as $offer)
                            <div class="box-offer">
                                <h3>{{ $type_order }} : {{ $offer->lb_subject }}</h3>
                                <p> {{ $offer->lb_desc }}</p>
                                <p> 
                                    {{ $offer->lb_country_name }} ( {{ $offer->created_at }})
                                </p>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
