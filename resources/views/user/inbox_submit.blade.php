@extends('layouts.inbox')

@section('content')
<div id="inbox-section">
    <div class="container">
        <div class="row">
            @if(Session::get('posting_message_success'))
                {{ Session::get('posting_message_success') }}
            @elseif(Session::get('posting_message_error'))
                {{ Session::get('posting_message_error') }}
            @endif
        </div>
    </div>
</div>
@endsection