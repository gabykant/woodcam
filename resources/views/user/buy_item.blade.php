@extends('layouts.guest')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="contact-box">
                <h2>Place an order to Buy</h2>

                @if(Session::get('order_offer_buy_fail'))
                    <p class="alert alert-danger">{{ Session::get('order_offer_buy_fail') }}</p>
                @endif
                <div class="form-box">
                    <form enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{ route('buyItemPost') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="order_type" value="buy" />
                        <div class="form-group{{ $errors->has('lb_subject') ? ' has-error' : '' }}">
                            <label for="lb_subject">Title</label>
                            <input type="text" name="lb_subject" class="form-control" value="{{ old('lb_subject') }}" />
                            @if ($errors->has('lb_subject'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('lb_subject') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_desc') ? ' has-error' : '' }}">
                            <label for="lb_desc" >Details</label>
                            <textarea rows="5" id="lb_desc" type="text" class="form-control" name="lb_desc" value="{{ old('lb_desc') }}" ></textarea>
                                @if ($errors->has('lb_desc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_desc') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_country_id') ? ' has-error' : '' }}">
                            <label for="lb_country_id" >Country</label>
                            <select id="country_list" name="lb_country_id" class="form-control">
                                @if(count($countries) > 0)
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->lb_country_name }} </option>
                                    @endforeach
                                @endif
                            </select>
                                @if ($errors->has('lb_country_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_country_id') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group btn-message">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-warning">
                                    Place an order
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection