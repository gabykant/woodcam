@extends('layouts.welcome')

@section('content')

<!-- Section Display Items -->
<div class="section_item">
    <div class="container">
        @if(Session::get('status'))
            {{ Session::get('status') }}
        @endif
    </div>    
</div>
<!-- End Section display items -->
@endsection
