@extends('layouts.welcome')

@section('content')

<!-- Section Display Items -->
<div class="section_item">
    <div class="container">    
        <form action="" method="post" class="form-horizontal">
            {{ csrf_field() }}
            <ul style="list-style: none;">
                <li><span style="font-weight: bold">Current balance:</span> ${{ Auth::user()->topup }}</li>
                <li><span style="font-weight: bold">Payment method</span>
                    <ul style="list-style: none;">
                        <li>
                            <label>
                                <input type="radio" value="card_payment" name="payment_mode"  />Card Payment</label><br />
                                <span id="payment_margin">We accept: Visa, MasterCard, American Express and Discover</span></li>
                        <li>
                            <label>
                                <input type="radio" value="paypal_payment" name="payment_mode"/>PayPal</label><br />
                                <span id="payment_margin">Accepts: Visa, MasterCard, American Express and Discover</span>
                        </li>
                        <li>
                            <label>
                                <input type="radio" value="orange_payment" name="payment_mode" />Orange Money
                            </label>
                        </li>
                        <li>
                            <label>
                                <input type="radio" value="mtn_payment" name="payment_mode"/>MTN Mobile Money
                            </label>
                        </li>
                    </ul>
                </li>
                <li><button type="submit" class="btn btn-warning">
                                    Next
                                </button></li>
            </ul>
        </form>
    </div>
</div>
<!-- End Section display items -->
@endsection