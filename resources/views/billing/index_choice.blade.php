@extends('layouts.welcome')

@section('content')

<!-- Section Display Items -->
<div class="section_item">
    <div class="container"> 
        <div class="rowd">   
            <form action="{{ url('/user/profile/billing/topupDeposit') }}" method="post" class="form-horizontal">
                {{ csrf_field() }}
                <input type="hidden" value="{{ $payment_mode }}" name="payment_mode">
                <div class="form-group{{ $errors->has('current_balance') ? ' has-error' : '' }}">
                    <div class="col-md-6 col-md-offset-2">
                        <label for="current_balance" >Current Balance: ${{ Auth::user()->topup }}</label>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                    <div class="col-md-6 col-md-offset-2">
                        <label for="amount" >Amount to Add ($) </label>
                        <input type="text" name="amount" id="amount" class="form-control" autofocus style="width: 250px" />
                        @if ($errors->has('amount'))
                            <span class="help-block">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('payment_mode') ? ' has-error' : '' }}">
                    <div class="col-md-6 col-md-offset-2">
                        <label for="payment_mode" >Payment Mode: {{ $payment_mode }} <a href="{{ url('/user/profile/billing/topup') }}"><i>Change</i></a></label>
                    </div>
                </div>

                <div class="form-group btn-message">
                    <div class="col-md-6 col-md-offset-2">
                        @if($payment_mode == 'PayPal')
                            <button type="submit" class="btn btn-default">
                                <img src="{{ asset('/images/money.png') }}" width="20px" /> Check Out with PayPal
                            </button>
                        @else
                            <button type="submit" class="btn btn-warning">
                                Send your message
                            </button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Section display items -->
@endsection