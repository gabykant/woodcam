@extends('layouts.welcome')

@section('content')

<!-- Section Display Items -->
<div class="section_items">
    <div class="container">
        <h3>Browse Wood Types
            <span class="pull-right"></span>
        </h3>
        <div id="browse-box">
            <div class="row">
                <div class="col-lg-3" style="border-right: 1px solid #ccc">
                    @if(count($woodtype) > 0)
                        <ul>
                        @foreach($woodtype as $wt)
                            <li style="padding: 5px 20px 10px;">
                                <a href="">{{ $wt->lb_type_name }}</a>
                            </li>
                        @endforeach
                        </ul>
                    @endif
                </div>
                <div class="col-lg-9">
                    <!-- Get results from database and put here -->
                    @if(count($orders) > 0)
                        @foreach($orders as $order)
                            <div class="col-lg-3 item_box">
                                
                                <div class="text-center">
                                    <p id="image_item">
                                        <a href="">
                                            <img src="{{ asset('storage/' . $order->lb_picture)}}" />
                                        </a>
                                    </p>
                                    <p>
                                        @if($order->lb_type_post == 'sell') 
                                            <label class="label label-info">Offer to Sell</label>
                                        @else
                                            <label class="label label-success">Offer to Buy</label>
                                        @endif
                                    </p>
                                    <p>
                                        <a href="">
                                            {{ str_limit( $order->lb_desc , 50) }}
                                        </a>
                                    </p>
                                    <span style="margin: 5px;">
                                        <label class="label label-info">
                                            Hint: {{ App\Models\Hint::where('lb_order_id', $order->id )->count() }}
                                        </label>
                                    </span>
                                    <span class="bottom-page"><a href="{{ url('/item/display/' . encrypt($order->id)) }}" class="btn btn-primary">View More</a></span>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Section display items -->
@endsection