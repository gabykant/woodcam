@extends('layouts.welcome')

@section('content')

<!-- Section Display Items -->
<div class="section_item">
    <div class="container">
        <h6>Home > 
            @if($order->lb_type_post == 'sell') 
                Sell
            @elseif($order->lb_type_post == 'buy')
                Buy
            @endif
            > <b>{{ $order->lb_subject }}</b>
            <span class="pull-right"></span>
        </h6>
        <div id="browse-item-box">
            <div class="row">
                <div class="col-lg-3">
                    <img src="{{ asset('storage/' . $order->lb_picture) }}" style="width: 23%"/>
                </div>
                <div class="col-lg-6">
                    <!-- Get results from database and put here -->
                    <h3>{{ $order->lb_subject }}</h3>
                    <span>By {{ $order->lb_user_id }}</span>

                    <div id="item_desc">
                        <h5 style="font-weight: bold">More details</h5>
                        <span> Type: {{ $order->lb_wood_type_id }}<br /></span>
                        <span> Description: {{ $order->lb_desc }}</span>
                    </div>

                    <div >
                        <a href="{{ url('/user/inbox/' . encrypt($order->id)) }}" class="btn btn-warning">Send your message</a>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div id="user-evaluation">
                        <p>
                            <b>{{ $user->USERNAME }}</b>
                        </p>
                        <p>    
                            <ul style="padding-left: 0px">
                                @if($ProfileVerification->lb_profile_info)
                                    <li title="User Profile Verified"><i class="glyphicon glyphicon-user" style="color: green"></i></li>
                                @else
                                    <li title="User Profile Unverified"><i class="glyphicon glyphicon-user"></i></li>
                                @endif

                                @if($ProfileVerification->lb_payment)
                                    <li title="Payment Capacities Verified"><i class="glyphicon glyphicon-usd" style="color: green"></i></li>
                                @else
                                    <li title="Payment Capacities Unverified"><i class="glyphicon glyphicon-usd"></i></li>
                                @endif

                                @if($ProfileVerification->lb_mailbox)
                                    <li title="Email Verified"><i class="glyphicon glyphicon-envelope" style="color: green"></i></li>
                                @else
                                    <li title="Email Unverified"><i class="glyphicon glyphicon-envelope"></i></li>
                                @endif

                                @if($ProfileVerification->lb_phone)
                                    <li title="Phone Number Verified"><i class="glyphicon glyphicon-earphone" style="color: green"></i></li>
                                @else
                                    <li title="Phone Number Unverified"><i class="glyphicon glyphicon-earphone"></i></li>
                                @endif

                                @if($ProfileVerification->lb_topup)
                                    <li title="Credit Card Verified"><i class="glyphicon glyphicon-credit-card" style="color: green"></i></li>
                                @else
                                    <li title="Credit Card Unverified"><i class="glyphicon glyphicon-credit-card"></i></li>
                                @endif
                            </ul>
                        </p>
                        <p>
                            Country, {{ $user->COUNTRY }}
                        </p>
                        <p>
                            Member since {{ substr($user->CREATEDAT, 0, 10) }}
                        </p>
                        <p>
                            0 Recommandations
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Section display items -->
@endsection