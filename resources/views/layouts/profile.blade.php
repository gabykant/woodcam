<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('/images/awt-logo-small.png') }}" width="50"/>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav" id="top-left">
                        <li>
                            What are you looking for ?
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">Buyers</a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('sellItem') }}">Place an order to Sell</a>
                                </li>
                                <li>
                                    <a href="">Search a Buyer</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">Suppliers</a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('buyItem') }}">Place an order to Buy</a>
                                </li>
                                <li>
                                    <a href="">Search a Supplier</a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('profile') }}">My Profile</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">My Profile</div>
                        <div class="panel-body">
                            <ul>
                                <li>
                                    <a href="">Personal data</a>
                                </li>
                                <li>
                                    <a href="">Edit Password</a>
                                </li>
                            </ul>
                        </div>
                        <div class="panel-heading">Company</div>
                        <div class="panel-body">
                            <ul>
                                <li>
                                    <a href="{{ url('/user/company/update') }}">Edit profile</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                @yield('content')

            </div>
        </div>

        <!-- Footer section -->
        <div class="footer">
            <div class="container">
                <div class="top-menu-footer">
                    <span ><img src="{{ asset('/images/logo-footer.png') }}" width="210"/></span>
                    <span class="footer-subheader">Africa Largest B2B Wood Trading platform for connecting Bullers and Sellers</span>
                </div>
                <div class="body-footer">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="{{ route('contact') }}">Contact Us
                        </div>
                        <div class="col-md-3"><a href="{{ route('contact') }}">Blog</a></div>
                        <div class="col-md-3"><a href="{{ route('about') }}">About Us</a></div>
                        <div class="col-md-3">
                        Stay in touch
                            <div class="footer-social" role="alert">
                                 <a href="{{ config('social.facebook.url') }}">
                                     <i class="fab fa-facebook-f"></i>
                                     Facebook
                                </a>
                                <a href="{{ config('social.twitter.url') }}">
                                     <i class="fab fa-facebook-f"></i>
                                     Twitter
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-footer">
                    &copy 2018 Ets. Africa Wood Trading. All rights reserved
                    <span class="pull-right" >
                        <ul style="list-style:none;">
                            <li>PayPal</li>
                            <li>Visa</li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
        <!-- End footer section -->
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        /*$(document).ready(function(){
            $.get('/user/countries/', {}, function(data){
                $.each(data, function(key, value){
                    $('#country').append($('<option>', {
                        value: value.id,
                        text: value.lb_country_name
                    }));
                });
            });

            $("#country").on('change', function(e){
                $('#city').html("");
                var selectedIndex = $("#country").val();
                $.get('/user/ports/' + selectedIndex, {}, function(data){
                    $.each(data, function(key, value){
                        $('#city').append($('<option>', {
                            value: value.id,
                            text: value.lb_port_name
                        }));
                    });
                });
            });
        });*/
    </script>

</body>
</html>
