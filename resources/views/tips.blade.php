@extends('layouts.welcome')

@section('content')

<div class="container">
    <h3>How to export wood from Cameroon ?
        <span class="pull-right"></span>
    </h3>

    <p>
        To export woods from Cameroon, there are requirements documents set by the Government.
    </p>

    <h4>Administrative documents</h4>
    <p>
        <ul>
            <li>Agr&eacute;ment en qualit&eacute; d'exploitant forestier</li>
            <li>Quitus fiscal valide</li>
            <li>Certificat de vente de coupe (ou autre titre d'exploitation)</li>
            <li>Certificat d'enregistrement en qualit&eacute; d'exploitant</li>
            <li>Certificat d'enregistrement en qualit&eacute; de transformateur</li>
            <li>Autorisation sp&eacute;ciale d'exploitation ( <i>des essences prohib&eacute;es. Exemple: Sappeli Grume, Iroko Grume, Padouk Grume, Accajou Grume, Bibolo Grume ...</i>)</li>
            <li>Lettre de voiture copie transporteur</li>
        </ul>
    </p>

    <h4>Custom documents</h4>
    <p>
        <ul>
            <li>Bulletin de Sp&eacute;cification</li>
            <li>Sp&eacute;cification</li>
            <li>Contrat</li>
            <li>Facture commerciale domicili&eacute;e</li>
            <li>Domiciliation d'exportation (Formule 1)</li>
            <li>Certificat d'empotage (Si exportation en conteneur)</li>
            <li>Rapport d'empotage des services des for&ecirc;ts</li>
            <li>Certificat d'origine formule A (Si destination Union Europ&eacute;enne)</li>
            <li>Certificat de circulation EUR1 (Si destination Union Europ&eacute;enne)</li>
            <li>Certificat d'origine Chambre de Commerce (hors Union Europ&eacute;enne)</li>
            <li>PAD (Port Autonome de Douala)</li>
            <li>Fiche Guiche Unique du Commerce Electronique (GUCE)</li>
            <li>BESC (Caisse Nationale des Chargeurs du Cameroun)</li>
            <li>D&eacute;clarations
                <ul>
                    <li>EX9 (si en conteneur)
                        <ul>
                            <li>9200</li>
                            <li>9182</li>
                        </ul>
                    </li>
                    <li>EX1 / EX3
                        <ul>
                            <li>3080 / 3181</li>
                            <li>1092 / 1000</li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </p>

    <h4>Specific cut wood document</h4>
    <p>
        <ul>
            <li>
                A. Requirement documents from exporter (A)
                <ul>
                    <li>Quitus fiscal (Direction G&eacute;n&eacute;rale des Imp&ocirc;ts)</li>
                    <li>Certificat d'enregistrement en qualit&eacute; d'exportateur</li>
                    <li>Certificat d'enregistrement en qualit&eacute; de transformateur</li>
                    <li>Patente</li>
                    <li>Registre de Commerce</li>
                    <li>Carte de Contribuable</li>
                </ul>
            </li>
            <li>
                B. Requirement documents for wood itself (B)
                <ul>
                    <li>Lettre de voiture vis&eacute;e par le D&eacute;l&eacute;gu&eacute; D&eacute;partemental</li>
                    <li>DIFOR (Bulletin de Sp&eacute;cification) d&eacute;livr&eacute; par le D&eacute;l&eacute;gu&eacute; Regional des for&ecirc;ts</li>
                    <li>Sp&eacute;cification (Exportateur)</li>
                    <li>Certificat d'empotage si bois en conteneurs</li>
                    <li>Rapport d'empotage (Eaux & For&ecirc;ts)</li>
                    <li>Certificats d'origine
                        <ul>
                            <li>Union Europ&eacute;enne : EUR1 / Formule A</li>
                            <li>Reste du monde: Certificat d'origine <b>Chambre du Commerce</b>
                        </ul>
                    </li>
                    <li>Facture commerciale domicili&eacute;e dans une banque</li>
                    <li>Domiciliation d'exportation ou Formule 1</li>
                    <li>Certificat PhytoSanitaire</li>
                    <li>Quittance de Paiement Redevances </b>PAD</b></li>
                    <li>Bordereau Electronique de Suivi des Cargaisons (BESC) d&eacute;livr&eacute; par le Conseil National des Chargeurs du Cameroun</li>
                    <li>Fiche de suivi des dossiers (GUCE)</li>
                </ul>
            </li>
        </ul>
    </p>

    <h4>Woods documents</h4>
    <p>
        <ul>
            <li>Requirement documents from <b>A</b> and <b>B</b></li>
            <li>BDT / AVE de la SGS</li>
        </ul>
    </p>
    
</div>
<!-- End Section display items -->
@endsection