@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <p class="alert alert-success">
                        Thank you for your registration<br /><br />
                        To be able to connect to our platform, you should click on the link sent to your email address to activate your account.
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
