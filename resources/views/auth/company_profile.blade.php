@extends('layouts.profile')

@section('content')
<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">Company update information</div>

        <div class="panel-body">

            @if(Session::get('company_update_success'))
                <p class="alert alert-success">{{ Session::get('company_update_success') }}</p>
            @elseif(Session::get('company_update_fail'))
                <p class="alert alert-danger">{{ Session::get('company_update_fail') }}</p>
            @endif
            <form enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{ url('/user/company/update') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('business_name') ? ' has-error' : '' }}">
                    <label for="business_name" class="col-md-4 control-label">Business Name</label>
                    <div class="col-md-6">
                        <input id="business_name" type="text" class="form-control" name="business_name" value="{{ $myCompany->business_name }}" />
                        @if ($errors->has('business_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('business_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('trade_number') ? ' has-error' : '' }}">
                    <label for="trade_number" class="col-md-4 control-label">Registration / Incorporate number</label>
                    <div class="col-md-6">
                        <input id="trade_number" type="text" class="form-control" name="trade_number" value="{{ $myCompany->trade_number }}" />
                        @if ($errors->has('trade_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('trade_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('tax_payer_id') ? ' has-error' : '' }}">
                    <label for="tax_payer_id" class="col-md-4 control-label">Tax Payer ID</label>
                    <div class="col-md-6">
                        <input id="tax_payer_id" type="text" class="form-control" name="tax_payer_id" value="{{ $myCompany->tax_payer_id }}" />
                        @if ($errors->has('tax_payer_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tax_payer_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('legal_form') ? ' has-error' : '' }}">
                    <label for="legal_form" class="col-md-4 control-label">Legal Form</label>
                    <div class="col-md-6">
                        <select name="legal_form" class="form-control">
                            @if(count($legal_form) > 0)
                                @foreach($legal_form as $legal)
                                    <option value="{{ $legal->id }}" {{ ($legal->id == $myCompany->legal_form ) ? "selected" : "" }} >
                                        {{ $legal->legal_display_name }} </option>
                                @endforeach
                            @endif
                            <option value="other">Others</option>
                        </select>
                        @if ($errors->has('legal_form'))
                            <span class="help-block">
                                <strong>{{ $errors->first('legal_form') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('url_logo') ? ' has-error' : '' }}">
                    <label for="url_logo" class="col-md-4 control-label">Company Logo</label>
                    <div class="col-md-6">
                        <input id="url_logo" type="file" class="form-control" name="url_logo" value="{{ old('url_logo') }}" />
                        @if ($errors->has('url_logo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('url_logo') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-2">
                        <img src="{{ asset('storage/' . $myCompany->url_logo ) }}" width="50px" />
                    </div>
                </div>

                <div class="form-group{{ $errors->has('creation_date') ? ' has-error' : '' }}">
                    <label for="creation_date" class="col-md-4 control-label">Business Creation Date</label>
                    <div class="col-md-6">
                        <input id="creation_date" type="date" class="form-control" name="creation_date" value="{{ $myCompany->creation_date }}" />
                        @if ($errors->has('creation_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('creation_date') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">Company Email Address</label>
                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $myCompany->email }}" />
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('phone_contact') ? ' has-error' : '' }}">
                    <label for="phone_contact" class="col-md-4 control-label">Company Contact</label>
                    <div class="col-md-6">
                        <input id="phone_contact" type="text" class="form-control" name="phone_contact" value="{{ $myCompany->phone_contact }}" />
                        @if ($errors->has('phone_contact'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone_contact') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                    <label for="lb_country" class="col-md-4 control-label">Incorporation Country</label>
                    <div class="col-md-6">
                        <select name="country" class="form-control" id="country">
                            @if(count($countries) > 0)
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" {{ ($country->id == $myCompany->country ) ? "selected" : "" }} />
                                        {{ $country->lb_country_name }} 
                                    </option>
                                @endforeach
                            @endif
                            <option value="other">Others</option>
                        </select>
                        @if ($errors->has('country'))
                            <span class="help-block">
                                <strong>{{ $errors->first('country') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('url_trade_number') ? ' has-error' : '' }}">
                    <label for="url_trade_number" class="col-md-4 control-label">Upload Trade Registration</label>
                    <div class="col-md-6">
                        <input id="url_trade_number" type="file" class="form-control" name="url_trade_number" value="{{ old('url_trade_number') }}" />
                        @if ($errors->has('url_trade_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('url_trade_number') }}</strong>
                            </span>
                        @endif
                    </div>
                    @if($myCompany->url_trade_number != "")
                        <div class="col-md-2"><a href="{{ asset('storage/' . $myCompany->url_trade_number ) }}" target="_blank">Open here</a></div>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('url_tax_payer') ? ' has-error' : '' }}">
                    <label for="url_tax_payer" class="col-md-4 control-label">Upload Tax Payer</label>
                    <div class="col-md-6">
                        <input id="url_tax_payer" type="file" class="form-control" name="url_tax_payer" value="{{ old('url_tax_payer') }}" />
                        @if ($errors->has('url_tax_payer'))
                            <span class="help-block">
                                <strong>{{ $errors->first('url_tax_payer') }}</strong>
                            </span>
                        @endif
                    </div>
                    @if($myCompany->url_tax_payer != "")
                        <div class="col-md-2"><a href="{{ asset('storage/' . $myCompany->url_tax_payer ) }}" >Open here</a></div>
                    @endif
                </div>

                <div class="form-group">
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary">
                            Update data
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection

