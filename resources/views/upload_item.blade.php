@extends('layouts.guest')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="contact-box">
                <h2>Place an order to sell</h2>

                @if(Session::get('post_selling_failed'))
                    <p class="alert alert-danger">{{ Session::get('post_selling_failed') }}</p>
                @endif
                
                <div class="form-box">
                    <form enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{ route('sellItemPost') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="order_type" value="sell" />
                        <div class="form-group{{ $errors->has('lb_wood_type_id') ? ' has-error' : '' }}">
                            <label for="lb_wood_type_id" >Choose categories</label>
                            <select name="lb_wood_type_id" class="form-control">
                                @if(count($type) > 0)
                                    @foreach($type as $t)
                                        <option value="{{ $t->id }}">{{ $t->lb_type_name }} </option>
                                    @endforeach
                                @endif
                                <option value=""></option>
                            </select>
                            @if ($errors->has('lb_wood_type_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('lb_wood_type_id') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_quantity') ? ' has-error' : '' }}">
                            <label for="lb_quantity">Quantity (/cm3)</label>
                            <input id="lb_quantity" type="text" class="form-control" name="lb_quantity" value="{{ old('lb_quantity') }}" >
                                @if ($errors->has('lb_quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_quantity') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_country_id') ? ' has-error' : '' }}">
                            <label for="lb_country_id" >Country</label>
                            <select id="country_list" name="lb_country_id" class="form-control">
                                @if(count($countries) > 0)
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->lb_country_name }} </option>
                                    @endforeach
                                @endif
                            </select>
                                @if ($errors->has('lb_country_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_country_id') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_delivery_port_id') ? ' has-error' : '' }}">
                            <label for="lb_delivery_port_id" >Delivery port</label>
                            <select id="lb_delivery_port_id" class="form-control" name="lb_delivery_port_id">
                                <option value=0>Select from the list</option>
                            </select>
                                @if ($errors->has('lb_delivery_port_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_delivery_port_id') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_subject') ? ' has-error' : '' }}">
                            <label for="lb_subject" >Subject</label>
                            <input id="lb_subject" type="text" class="form-control" name="lb_subject" value="{{ old('lb_subject') }}" >
                                @if ($errors->has('lb_subject'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_subject') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_desc') ? ' has-error' : '' }}">
                            <label for="lb_desc" >Details</label>
                            <textarea rows="5" id="lb_desc" type="text" class="form-control" name="lb_desc" value="{{ old('lb_desc') }}" ></textarea>
                                @if ($errors->has('lb_desc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_desc') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_picture') ? ' has-error' : '' }}">
                            <label for="lb_picture" >Upload image</label>
                            <input id="lb_picture" type="file" class="form-control" name="lb_picture" value="{{ old('lb_picture') }}" />
                                @if ($errors->has('lb_picture'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_picture') }}</strong>
                                    </span>
                                @endif
                        </div>
 
                        <div class="form-group btn-message">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-warning">
                                    Place an order
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection