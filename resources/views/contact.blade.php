@extends('layouts.guest')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="contact-box">
                <h2>Contact</h2>

                @if(Session::get('save_contact_success'))
                    <p class="alert alert-success">
                        {{ Session::get('save_contact_success') }}
                    </p>
                @elseif(Session::get('save_contact_fail'))
                <p class="alert alert-danger">
                        {{ Session::get('save_contact_fail') }}
                    </p>
                @endif
                
                <div class="form-box">
                    <form class="form-horizontal" method="POST" action="{{ route('contact') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                            <label for="fullname" >Full Name</label>
                            <input id="fullname" type="text" class="form-control" name="fullname" value="{{ old('fullname') }}" >
                                @if ($errors->has('fullname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fullname') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_email') ? ' has-error' : '' }}">
                            <label for="lb_email" >Email Address</label>
                            <input id="lb_email" type="text" class="form-control" name="lb_email" value="{{ old('lb_email') }}" >
                                @if ($errors->has('lb_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_subject') ? ' has-error' : '' }}">
                            <label for="lb_subject" >Subject</label>
                            <input id="lb_subject" type="text" class="form-control" name="lb_subject" value="{{ old('lb_subject') }}">
                                @if ($errors->has('lb_subject'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_subject') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('lb_text') ? ' has-error' : '' }}">
                            <label for="lb_text" >Message</label>
                            <textarea id="lb_text" rows="5" class="form-control" name="lb_text" value="{{ old('lb_text') }}" ></textarea>
                                @if ($errors->has('lb_text'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lb_text') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group btn-message">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-warning">
                                    Send your message
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="contact-box-info">
                <h5>Contact us</h5>
                <ul style="list-style-typ: tick">
                    <li>Alert an abuse</li>
                    <li>Request an order</li>
                    <li>Discuss with an agent</li>
                    <li>Also on WhatsApp +237 677 31 51 45</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection