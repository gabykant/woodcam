<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'GuestController@index');
Route::get('/item/display/{order_id}','GuestController@display_order');

// For Guest user only
Route::get('/contact', 'GuestController@contact_us')->name('contact');
Route::post('/contact', 'GuestController@contact_us')->name('contact');
Route::get('/about', 'GuestController@about_us')->name('about');
Route::get('/tips-export', 'GuestController@tips')->name('tips-export');
Route::get('/woods-forbidden', 'GuestController@forbidden')->name('woods-forbidden');

Route::post('/q', 'GuestController@search_item')->name('searchItemPost');

Auth::routes();

Route::group(['middleware' => ['auth']], function($route){
    $route->get('/user/country/{id}', 'User\DashboardController@getCountry');
    $route->get('/user/countries', 'User\DashboardController@getCountries');
    $route->get('/user/ports/{country_id}', 'User\DashboardController@getPorts');
    $route->get('/user/dashboard', 'User\DashboardController@index');
    $route->get('/user/inbox/{order_id}', 'User\DashboardController@inbox');
    //$route->post('/user/inbox/{order_id}', 'User\DashboardController@inbox');
    $route->post('/user/inbox/add', 'User\DashboardController@inbox_submit');
    $route->get('/user/profile/billing/topup', 'User\BillingController@index');
    $route->post('/user/profile/billing/topup', 'User\BillingController@index');
    $route->get('/user/profile/billing/topupWith/{choice}', 'User\BillingController@index_choice');
    $route->post('/user/profile/billing/topupDeposit', 'User\BillingController@index_deposit');

    $route->post('/user/paypal/postPayment', 'User\BillingController@paypal_post_payment');

    $route->get('/user/paypal/return', 'User\BillingController@paypal_payment_status');
    $route->get('/user/paypal/cancel', 'User\BillingController@after_cancel');

    $route->get('/user/acccount/topup', 'User\BillingController@top_up_load_success');

    $route->get('/user/company/update', 'User\DashboardController@company_profile');
    $route->post('/user/company/update', 'User\DashboardController@company_profile');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account/creation', 'GuestController@account_creation');

// User profile
Route::get('/user/profile', 'Auth\ProfileController@index')->name('profile');

// User Dashboard
Route::get('/user/dashboard/upload_item', 'User\DashboardController@index_sell')->name('sellItem');
Route::post('/user/dashboard/upload_item', 'User\DashboardController@index_sell')->name('sellItemPost');
Route::get('/user/dashboard/deal', 'User\DashboardController@index_buy')->name('buyItem');
Route::post('/user/dashboard/deal', 'User\DashboardController@index_buy')->name('buyItemPost');
Route::get('/user/dashboard/list_buy', 'User\DashboardController@index_after_submit_buy');

Route::group(['middleware' => ['auth', 'admin']], function($route){
    $route->get('/admin', 'Admin\PanelAdminController@index');
});