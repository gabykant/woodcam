<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecordPaypalTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recordPayPalTransaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lb_transaction_id', 250);
            $table->string('lb_payer_email', 250);
            $table->string('lb_payer_firstname', 250);
            $table->string('lb_payer_lastname', 250);
            $table->string('lb_payer_id', 250);
            $table->double('lb_montant');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recordPayPalTransaction');
    }
}
