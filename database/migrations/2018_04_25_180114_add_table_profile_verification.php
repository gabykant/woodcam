<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableProfileVerification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profileverification', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("lb_user_id")->unsigned();
            $table->boolean("lb_profile_info")->default(0);
            $table->boolean("lb_payment")->default(0);
            $table->boolean("lb_mailbox")->default(0);
            $table->boolean("lb_phone")->default(0);
            $table->boolean("lb_topup")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profileverification');
    }
}
