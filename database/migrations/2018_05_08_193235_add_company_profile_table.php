<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companyProfile', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_name', 250);
            $table->string('trade_number', 250);
            $table->string('tax_payer_id', 250)->unique();
            $table->string('legal_form', 250);
            $table->string('url_logo');
            $table->date('creation_date');
            $table->string('email', 250);
            $table->string('phone_contact', 250);
            $table->integer('country')->unsigned();
            $table->integer('city')->unsigned();
            $table->string('url_trade_number', 250);
            $table->string('url_tax_payer', 250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companyProfile');
    }
}
