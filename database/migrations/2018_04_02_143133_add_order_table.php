<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lb_user_id')->unsigned();
            $table->enum('lb_type_post', ['sell', 'buy', 'both']);
            $table->integer('lb_wood_type_id')->unsigned();
            $table->integer('lb_quantity');
            $table->integer('lb_country_id')->unsigned();
            $table->integer('lb_delivery_port_id')->unsigned();
            $table->string('lb_subject', 250);
            $table->string('lb_desc', 250);
            $table->string('lb_picture', 250);
            $table->timestamps();

            $table->foreign('lb_user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('lb_wood_type_id')->references('id')->on('woodtype')->onDelete('set null');
            $table->foreign('lb_country_id')->references('id')->on('country')->onDelete('set null');
            $table->foreign('lb_delivery_port_id')->references('id')->on('ports')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
