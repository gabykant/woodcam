<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLegalFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legalform', function (Blueprint $table) {
            $table->increments('id');
            $table->string('legal_slug_name', 50);
            $table->string('legal_display_name', 250);
            $table->timestamps();
        });

        DB::table('legalform')->insert(
            array(
                array(
                    'legal_slug_name' => 'ets',
                    'legal_display_name' => 'Ets'
                ),
                array(
                    'legal_slug_name' => 'sarl',
                    'legal_display_name' => 'Sarl'
                ),
                array(
                    'legal_slug_name' => 'sa',
                    'legal_display_name' => 'SA'
                )
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legalform');
    }
}
